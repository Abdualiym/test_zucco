<?php

namespace Domain\Schedule\Repositories;

use Domain\Schedule\Entities\Schedule;
use Illuminate\Database\Eloquent\Model;

class ScheduleRepository
{
    /**
     * @var Schedule|null
     */
    protected ?Schedule $schedules;

    /**
     * @param Schedule $schedules
     */
    public function __construct(Schedule $schedules)
    {
        return $this->schedules = $schedules;
    }

    /**
     * @param array $data
     * @param Model $schedules
     * @return Model
     */
    public function filter(array $data, Model $schedules)
    {
        $schedules = isset($data['id'])
            ?  $schedules->where('id', $data['id'])
            :$schedules;
        $schedules = isset($data['name'])
            ?  $schedules->where('name', $data['name'])
            :$schedules;
        $schedules = isset($data['created_at'])
            ?  $schedules->where('created_at', $data['created_at'])
            :$schedules;
        return $schedules;
    }
}
