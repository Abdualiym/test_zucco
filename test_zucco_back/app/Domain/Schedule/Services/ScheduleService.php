<?php

namespace Domain\Schedule\Services;

use Illuminate\Support\Facades\DB;
use Domain\Schedule\DTO\ScheduleDTO;
use Domain\Schedule\Entities\Schedule;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\Domain\Schedule\DTO\ScheduleFilterDTO;
use Domain\Schedule\Interfaces\ScheduleInterface;
use Domain\Schedule\Repositories\ScheduleRepository;

/**
 * class ScheduleService
 * @package Domain\Schedule\Services
 *
 * @author Mirkomil Saitov <mirkomilmirabdullaevich@gmail.com>
 *
 * @property Schedule $room
 * @property ScheduleRepository $roomRepository
 */
class ScheduleService implements ScheduleInterface
{
    /**
     * @var Schedule
     */
    protected Schedule $rooms;

    /**
     * @var ScheduleRepository $roomRepository
     */
    protected ScheduleRepository $roomRepository;

    /**
     * @param Schedule $rooms
     * @param ScheduleRepository $roomRepository
     */
    public function __construct(Schedule $rooms, ScheduleRepository $roomRepository)
    {
        $this->roomRepository = $roomRepository;
        $this->rooms = $rooms;
    }

    /**
     * @param array $data
     * @return Builder
     */
    public function indexes(ScheduleFilterDTO $dto): Builder
    {
        return $this->roomRepository->filter($dto->toArray(), $this->rooms)->orderBy('id', 'desc');
    }

    /**
     * @param ScheduleDTO $dto
     * @return Model
     * @throws \Exception
     */
    public function storeSchedule(ScheduleDTO $dto): Model
    {
        DB::beginTransaction();
        try {
            $data = self::getDto($dto);

            $rooms = $this->rooms->create($data);

            DB::commit();
        } catch (\Throwable $exception) {
            DB::rollBack();
            throw new \Exception('On creating Schedule ' . $exception->getMessage(), 500);
        }
        return $rooms;
    }

    /**
     * @param int $id
     * @return Model
     */
    public function getModelById(int $id): Model
    {
        return $this->rooms->findOrFail($id);
    }

    /**
     * @param ScheduleDTO $dto
     * @param int $id
     * @return bool
     */
    public function updateSchedule(ScheduleDTO $dto, int $id): bool
    {
        DB::beginTransaction();
        try {
            $room = $this->rooms->findOrFail($id);

            throw_if(is_null($room), new \Exception("Schedule not found $id"));

            $data = self::getDto($dto);

            $room->update($data);

            DB::commit();
        } catch (\Throwable $exception) {
            DB::rollBack();
            throw new \Exception('On updating Schedules ' . $exception->getMessage(), 500);
        }
        return true;

    }

    /**
     * @param int $id
     * @return bool
     */
    public function deleteSchedule(int $id): bool
    {
        try {
            $room = $this->rooms->findOrFail($id);
            if (!is_null($room))
                return $room->delete();
            return false;
        } catch (\Throwable $exception) {
            throw new \Exception('On deleting Schedule' . $exception->getMessage(), 500);
        }
    }


    /**
     * @param ScheduleDTO $dto
     * @return array
     */
    public static function getDto(ScheduleDTO $dto): array
    {
        return $dto->toArray();
    }
}
