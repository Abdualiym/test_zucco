<?php

namespace Domain\Core;

interface Attribute
{
    const ID            = 'id';
    const NAME          = 'name';
    const TITLE         = 'title';
    const CODE          = 'code';
    const DESCRIPTION   = 'description';
    const CREATED_BY    = 'user_id';
    const IS_ACTIVE     = 'is_active';
    const IS_DEFAULT    = 'is_default';
    const IS_DELETED    = 'is_deleted';
    const IS_BLOCKED    = 'is_blocked';

    const STATUSES  = [
        'active'=>'ACTIVE', 'blocked'=> 'BLOCKED', 'deleted' => 'DELETED'
    ];

}
