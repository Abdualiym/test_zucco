<?php

namespace Domain\User\Entities;

use Domain\Core\Authenticatable;
use Domain\User\Interfaces\UserAttribute;

/**
 * class User
 * @package Domain\User\Entities
 *
 * @author Mirkomil Saitov <mirkomilmirabdullaevich@gmail.com>
 *
 */
class User extends Authenticatable implements  UserAttribute
{
    public $table  = self::TABLE;
}
