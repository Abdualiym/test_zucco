<?php

namespace Domain\User\Services;

use App\Domain\User\DTO\UserFilterDTO;
use Domain\Schedule\Entities\Schedule;
use Domain\User\DTO\UserDTO;
use Domain\User\Entities\User;
use Domain\User\Interfaces\UserInterface;
use Domain\User\Repositories\UserRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

/**
 * class UserService
 * @package Domain\User\Services
 *
 * @author Mirkomil Saitov <mirkomilmirabdullaevich@gmail.com>
 *
 * @property User $user
 * @property UserRepository $userRepository
 */
class UserService implements UserInterface
{
    /**
     * @var User
     */
    protected User $users;

    /**
     * @var UserRepository
     */
    protected UserRepository $userRepository;

    /**
     * @param User $users
     * @param UserRepository $userRepository
     */
    public function __construct(User $users, UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
        $this->users = $users;
    }

    /**
     * @param UserFilterDTO $dto
     * @return Builder
     */
    public function indexes(UserFilterDTO $dto): Builder
    {
        return $this->userRepository->filter($dto->toArray(), $this->users)->orderBy('id', 'desc');
    }

    public function filterBySchedule(array $data)
    {
        $start_date = $data['start_date'] ?? null;
        if ($start_date){
            $schedule = Schedule::query()->where('start_date','=', $start_date)->first();
            if ($schedule){
                return $this->users->where('id', '!=', $schedule->room_id);
            }
        }
        return  $this->users;
    }

    /**
     * @param UserDTO $dto
     * @return Model
     */
    public function storeUser(UserDTO $dto): Model
    {
        DB::beginTransaction();
        try {
           $data = self::getDto($dto);

            $users = $this->users->create($data);

            DB::commit();
        } catch (\Throwable $exception) {
            DB::rollBack();
            throw new \Exception('On creating User ' . $exception->getMessage(), 500);
        }
        return $users;
    }

    /**
     * @param UserDTO $dto
     * @param int $id
     * @return bool
     */
    public function updateUser(UserDTO $dto, int $id): bool
    {
        DB::beginTransaction();
        try {
            $user = $this->users->findOrFail($id);

            throw_if(is_null($user), new \Exception("User not found $id"));

            $data = self::getDto($dto);
            $user->update($data);

            DB::commit();
        } catch (\Throwable $exception) {
            DB::rollBack();
            throw new \Exception('On updating Users ' . $exception->getMessage(), 500);
        }
        return true;

    }

    /**
     * @param int $id
     * @return bool
     */
    public function deleteUser(int $id): bool
    {
        try {
            $user = $this->users->findOrFail($id);
            if(!is_null($user))
                return $user->delete();
            return false;
        } catch (\Throwable $exception) {
            throw new \Exception('On deleting User' . $exception->getMessage(), 500);
        }
    }


    /**
     * @param UserDTO $dto
     * @return array
     */
    public static function getDto(UserDTO $dto): array
    {
        return $dto->toArray();
    }
}
