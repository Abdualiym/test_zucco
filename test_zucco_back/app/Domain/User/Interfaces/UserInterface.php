<?php

namespace Domain\User\Interfaces;

use Domain\User\DTO\UserDTO;
use Illuminate\Database\Eloquent\Model;

/**
 * interface UserInterface
 * @package Domain\User\Interfaces
 *
 * @author Mirkomil Saitov <mirkomilmirabdullaevich@gmail.com>
 */
interface UserInterface
{
    /**
     * @param UserDTO $dto
     * @return Model
     */
    public function storeUser(UserDTO $dto) :Model;

    /**
     * @param UserDTO $dto
     * @param int $id
     * @return bool
     */
    public function updateUser(UserDTO $dto, int $id) :bool;

    /**
     * @param int $id
     * @return bool
     */
    public function deleteUser(int $id): bool;
}
