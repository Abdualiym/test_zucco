<?php

namespace Domain\Group\Services;

use Domain\Schedule\Entities\Schedule;
use Illuminate\Support\Facades\DB;
use Domain\Group\DTO\GroupDTO;
use Domain\Group\Entities\Group;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\Domain\Group\DTO\GroupFilterDTO;
use Domain\Group\Interfaces\GroupInterface;
use Domain\Group\Repositories\GroupRepository;

/**
 * class GroupService
 * @package Domain\Group\Services
 *
 * @author Mirkomil Saitov <mirkomilmirabdullaevich@gmail.com>
 *
 * @property Group $group
 * @property GroupRepository $groupRepository
 */
class GroupService implements GroupInterface
{
    /**
     * @var Group
     */
    protected Group $groups;

    /**
     * @var GroupRepository $groupRepository
     */
    protected GroupRepository $groupRepository;

    /**
     * @param Group $groups
     * @param GroupRepository $groupRepository
     */
    public function __construct(Group $groups, GroupRepository $groupRepository)
    {
        $this->groupRepository = $groupRepository;
        $this->groups = $groups;
    }

    /**
     * @param array $data
     * @return Builder
     */
    public function indexes(GroupFilterDTO $dto): Builder
    {
        return $this->groupRepository->filter($dto->toArray(), $this->groups)->orderBy('id', 'desc');
    }

    public function filterBySchedule(array $data)
    {
        $start_date = $data['start_date'] ?? null;
        if ($start_date){
            $schedule = Schedule::query()->where('start_date','=', $start_date)->first();
            if ($schedule){
                return $this->groups->where('id', '!=', $schedule->room_id);
            }
        }
        return  $this->groups;
    }

    /**
     * @param GroupDTO $dto
     * @return Model
     * @throws \Exception
     */
    public function storeGroup(GroupDTO $dto): Model
    {
        DB::beginTransaction();
        try {
            $data = self::getDto($dto);

            $groups = $this->groups->create($data);

            DB::commit();
        } catch (\Throwable $exception) {
            DB::rollBack();
            throw new \Exception('On creating Group ' . $exception->getMessage(), 500);
        }
        return $groups;
    }

    /**
     * @param int $id
     * @return Model
     */
    public function getModelById(int $id): Model
    {
        return $this->groups->findOrFail($id);
    }

    /**
     * @param GroupDTO $dto
     * @param int $id
     * @return bool
     */
    public function updateGroup(GroupDTO $dto, int $id): bool
    {
        DB::beginTransaction();
        try {
            $group = $this->groups->findOrFail($id);

            throw_if(is_null($group), new \Exception("Group not found $id"));

            $data = self::getDto($dto);

            $group->update($data);

            DB::commit();
        } catch (\Throwable $exception) {
            DB::rollBack();
            throw new \Exception('On updating Groups ' . $exception->getMessage(), 500);
        }
        return true;

    }

    /**
     * @param int $id
     * @return bool
     */
    public function deleteGroup(int $id): bool
    {
        try {
            $group = $this->groups->findOrFail($id);
            if (!is_null($group))
                return $group->delete();
            return false;
        } catch (\Throwable $exception) {
            throw new \Exception('On deleting Group' . $exception->getMessage(), 500);
        }
    }


    /**
     * @param GroupDTO $dto
     * @return array
     */
    public static function getDto(GroupDTO $dto): array
    {
        return $dto->toArray();
    }
}
