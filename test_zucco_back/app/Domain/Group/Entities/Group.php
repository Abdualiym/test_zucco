<?php

namespace Domain\Group\Entities;

use Domain\Core\Entity;
use Domain\Group\Interfaces\GroupAttribute;

/**
 * class Group
 * @package Domain\Group\Entities
 *
 * @author Mirkomil Saitov <mirkomilmirabdullaevich@gmail.com>
 *
 */
class Group extends Entity implements GroupAttribute
{
    public $table  = self::TABLE;

}
