<?php

namespace Domain\Group\Repositories;

use Domain\Group\Entities\Group;
use Illuminate\Database\Eloquent\Model;

class GroupRepository
{
    /**
     * @var Group|null
     */
    protected ?Group $groups;

    /**
     * @param Group $groups
     */
    public function __construct(Group $groups)
    {
        return $this->groups = $groups;
    }

    /**
     * @param array $data
     * @param Model $groups
     * @return Model
     */
    public function filter(array $data, Model $groups)
    {
        $groups = isset($data['id'])
            ?  $groups->where('id', $data['id'])
            :$groups;
        $groups = isset($data['name'])
            ?  $groups->where('name', $data['name'])
            :$groups;
        $groups = isset($data['created_at'])
            ?  $groups->where('created_at', $data['created_at'])
            :$groups;
        return $groups;
    }
}
