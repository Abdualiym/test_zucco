<?php

namespace Domain\Room\Repositories;

use Domain\Room\Entities\Room;
use Illuminate\Database\Eloquent\Model;

class RoomRepository
{
    /**
     * @var Room|null
     */
    protected ?Room $rooms;

    /**
     * @param Room $rooms
     */
    public function __construct(Room $rooms)
    {
        return $this->rooms = $rooms;
    }

    /**
     * @param array $data
     * @param Model $rooms
     * @return Model
     */
    public function filter(array $data, Model $rooms)
    {
        $rooms = isset($data['id'])
            ?  $rooms->where('id', $data['id'])
            :$rooms;
        $rooms = isset($data['name'])
            ?  $rooms->where('name', $data['name'])
            :$rooms;
        $rooms = isset($data['created_at'])
            ?  $rooms->where('created_at', $data['created_at'])
            :$rooms;
        return $rooms;
    }
}
