<?php

namespace Domain\Subject\Interfaces;

use Domain\Core\Attribute;

interface SubjectAttribute
{
    const TABLE    = 'subjects';
}
