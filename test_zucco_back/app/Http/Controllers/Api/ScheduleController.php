<?php

namespace App\Http\Controllers\Api;

use App\Domain\Schedule\DTO\ScheduleFilterDTO;
use App\Http\Controllers\Controller;
use App\Http\Requests\Schedule\FilterRequest;
use App\Http\Requests\Schedule\StoreRequest;
use App\Http\Requests\Schedule\UpdateRequest;
use App\Http\Resources\Schedule;
use App\Http\Resources\ScheduleCollection;
use Domain\Schedule\DTO\ScheduleDTO;
use Domain\Schedule\Services\ScheduleService;
use Illuminate\Http\JsonResponse;

class ScheduleController extends Controller
{
    protected ScheduleService $service;

    public function __construct(ScheduleService $service)
    {
        $this->service = $service;
    }

    /**
     * @param FilterRequest $request
     * @return JsonResponse
     */
    public function index(FilterRequest $request): JsonResponse
    {
        $services = $this->service->indexes(ScheduleFilterDTO::fromRequest($request));

        $services = $services->paginate(self::PER_PAGE);
        return response()->json([
            'schedules' => new ScheduleCollection($services)
        ], 200);
    }

    /**
     * @param StoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(StoreRequest $request): \Illuminate\Http\JsonResponse
    {
        $service = $this->service->storeSchedule(ScheduleDTO::fromRequest($request));

        return response()->json([
            'schedule' => new Schedule($service)
        ], 200);
    }

    /**
     * @param UpdateRequest $request
     * @param $id
     * @return JsonResponse
     * @throws \Exception
     */
    public function update(UpdateRequest $request, int $id): \Illuminate\Http\JsonResponse
    {
        $service = $this->service->updateSchedule(ScheduleDTO::fromRequest($request), $id);

        if ($service){
            return response()->json([
                'schedule' => 'successfully updated'
            ], 200);
        }
        return \response()->json('error occurred');
    }

    /**
     * @param int $id
     * @return JsonResponse
     * @throws \Exception
     */
    public function destroy(int $id): \Illuminate\Http\JsonResponse
    {
        $service = $this->service->deleteSchedule($id);

        if ($service){
            return response()->json([
                'schedule' => 'successfully deleted'
            ], 200);
        }
        return \response()->json('error occurred');


    }
}
