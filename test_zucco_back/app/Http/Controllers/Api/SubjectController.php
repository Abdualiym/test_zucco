<?php

namespace App\Http\Controllers\Api;

use App\Domain\Subject\DTO\SubjectFilterDTO;
use App\Http\Controllers\Controller;
use App\Http\Requests\Subject\FilterRequest;
use App\Http\Requests\Subject\StoreRequest;
use App\Http\Requests\Subject\UpdateRequest;
use App\Http\Resources\Subject;
use App\Http\Resources\SubjectCollection;
use Domain\Subject\DTO\SubjectDTO;
use Domain\Subject\Services\SubjectService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Symfony\Component\HttpFoundation\Response;

class SubjectController extends Controller
{
    protected SubjectService $service;

    public function __construct(SubjectService $service)
    {
        $this->service = $service;
    }

    /**
     * @param FilterRequest $request
     * @return JsonResponse
     */
    public function index(FilterRequest $request): JsonResponse
    {
        $services = $this->service->indexes(SubjectFilterDTO::fromRequest($request));

        $services = $services->paginate(self::PER_PAGE);

        return response()->json([
            'subjects' => new SubjectCollection($services)
        ], 200);
    }

    public function inventory(Request $request)
    {
        logger($request->all());
        $services = $this->service->filterBySchedule($request->all());

        $services = $services->get();

        return response()->json([
            'subjects' => $services
        ], 200);
    }


    /**
     * @param StoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(StoreRequest $request): \Illuminate\Http\JsonResponse
    {
        $service = $this->service->storeSubject(SubjectDTO::fromRequest($request));

        return response()->json([
            'subject' => new Subject($service)
        ], 200);
    }

    /**
     * @param UpdateRequest $request
     * @param $id
     * @return JsonResponse
     * @throws \Exception
     */
    public function update(UpdateRequest $request, int $id): \Illuminate\Http\JsonResponse
    {
        $service = $this->service->updateSubject(SubjectDTO::fromRequest($request), $id);

        if ($service){
            return response()->json([
                'subject' => 'successfully updated'
            ], 200);
        }
        return \response()->json('error occurred');
    }

    /**
     * @param int $id
     * @return JsonResponse
     * @throws \Exception
     */
    public function destroy(int $id): \Illuminate\Http\JsonResponse
    {
        $service = $this->service->deleteSubject($id);

        if ($service){
            return response()->json([
                'subject' => 'successfully deleted'
            ], 200);
        }
        return \response()->json('error occurred');


    }
}
