<?php

namespace App\Http\Controllers\Api;

use App\Domain\User\DTO\UserFilterDTO;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\FilterRequest;
use App\Http\Requests\User\StoreRequest;
use App\Http\Requests\User\UpdateRequest;
use App\Http\Resources\User;
use App\Http\Resources\UserCollection;
use Domain\User\DTO\UserDTO;
use Domain\User\Services\UserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Symfony\Component\HttpFoundation\Response;

class UserController extends Controller
{
    protected UserService $service;

    public function __construct(UserService $service)
    {
        $this->service = $service;
    }

    /**
     * @param FilterRequest $request
     * @return JsonResponse
     */
    public function index(FilterRequest $request): JsonResponse
    {
        $services = $this->service->indexes(UserFilterDTO::fromRequest($request));

        $services = $services->paginate(self::PER_PAGE);

        return response()->json([
            'teachers' => new UserCollection($services)
        ], 200);
    }

    public function inventory(Request $request)
    {
        logger($request->all());
        $services = $this->service->filterBySchedule($request->all());

        $services = $services->get();

        return response()->json([
            'teachers' => $services
        ], 200);
    }


    /**
     * @param StoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(StoreRequest $request): \Illuminate\Http\JsonResponse
    {
        $service = $this->service->storeUser(UserDTO::fromRequest($request));

        return response()->json([
            'teacher' => new User($service)
        ], 200);
    }

    /**
     * @param UpdateRequest $request
     * @param $id
     * @return JsonResponse
     * @throws \Exception
     */
    public function update(UpdateRequest $request, int $id): \Illuminate\Http\JsonResponse
    {
        $service = $this->service->updateUser(UserDTO::fromRequest($request), $id);

        if ($service){
            return response()->json([
                'teacher' => 'successfully updated'
            ], 200);
        }
        return \response()->json('error occurred');
    }

    /**
     * @param int $id
     * @return JsonResponse
     * @throws \Exception
     */
    public function destroy(int $id): \Illuminate\Http\JsonResponse
    {
        $service = $this->service->deleteUser($id);

        if ($service){
            return response()->json([
                'teacher' => 'successfully deleted'
            ], 200);
        }
        return \response()->json('error occurred');


    }
}
