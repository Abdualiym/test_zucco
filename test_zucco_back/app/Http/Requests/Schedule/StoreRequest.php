<?php

namespace App\Http\Requests\Schedule;

use Domain\Core\BaseRequest;

class StoreRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'teacher_id' => 'required',
            'group_id' => 'required',
            'room_id' => 'required',
            'subject_id' => 'required',
            'start_date' => 'required',
        ];
    }
}
