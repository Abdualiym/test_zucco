<?php

namespace App\Http\Requests\User;

use Domain\Core\BaseRequest;

class StoreRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'full_name'  => 'required'
        ];
    }
}
