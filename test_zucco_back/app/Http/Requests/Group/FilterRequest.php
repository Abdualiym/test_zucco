<?php

namespace App\Http\Requests\Group;

use Domain\Core\BaseRequest;

class FilterRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'nullable',
            'id' => 'nullable',
            'created_at' => 'nullable'
        ];
    }
}
