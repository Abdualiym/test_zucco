export default {
    data() {
        return {
            loadingButton: false,
            form: {},
        }
    },
    created() {
        if (this.model != null) {
            this.form = JSON.parse(JSON.stringify(this.model));
        }
    },
    methods: {
        parent() {
            return this.$parent.$parent
        },
        reloadForm() {
            if (this.model != null) {
                this.form = JSON.parse(JSON.stringify(this.model));
            }
        }
    },
    watch: {
        model: {
            handler: function (newVal, oldVal) {
                this.reloadForm();
            },
            deep: true,
            immediate: true
        }
    }
}