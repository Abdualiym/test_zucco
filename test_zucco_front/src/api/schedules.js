import request from '@/utils/request'


export function index(params) {
    return request({
        url: '/schedules',
        method: 'get',
        params
    })
}

export function store(data) {
    return request({
        url: '/schedules',
        method: 'post',
        data
    })
}

export function update(data) {
    return request({
        url: `/schedules/${data.id}`,
        method: 'put',
        data
    })
}

export function destroy(id) {
    return request({
        url: `/schedules/${id}`,
        method: 'delete',
    })
}
