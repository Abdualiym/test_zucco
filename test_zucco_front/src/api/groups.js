import request from '@/utils/request'


export function index(params) {
    return request({
        url: '/groups',
        method: 'get',
        params
    })
}
export function inventory(params) {
    return request({
        url: '/groups_inventory',
        method: 'get',
        params
    })
}
export function store(data) {
    return request({
        url: '/groups',
        method: 'post',
        data
    })
}

export function update(data) {
    return request({
        url: `/groups/${data.id}`,
        method: 'put',
        data
    })
}

export function destroy(id) {
    return request({
        url: `/groups/${id}`,
        method: 'delete',
    })
}
