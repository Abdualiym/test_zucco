import { actions } from './room/actions'
import { getters } from './room/getters'
import { state } from './room/state'
import { mutations } from './room/mutations'

export default {
    namespaced: true,
    getters,
    state,
    mutations,
    actions
}
