export const columns = {
    id: {
        show: true,
        title: "№",
        sortable: true,
        column: 'id'
    },

    start_date: {
        show: true,
        title: "Start date",
        sortable: true,
        column: 'start_date'
    },
    room_id: {
        show: true,
        title: "Room",
        sortable: true,
        column: 'room_id'
    },
    group_id: {
        show: true,
        title: "Group",
        sortable: true,
        column: 'group_id'
    },
    teacher_id: {
        show: true,
        title: "Teacher",
        sortable: true,
        column: 'teacher_id'
    },
    subject_id: {
        show: true,
        title: "Subject",
        sortable: true,
        column: 'subject_id'
    },
    created_at: {
        show: true,
        title: "Created at",
        sortable: true,
        column: 'created_at'
    },
    updated_at: {
        show: false,
        title: "Updated at",
        sortable: true,
        column: 'updated_at'
    }
};
