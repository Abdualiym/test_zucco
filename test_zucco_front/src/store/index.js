import Vue from 'vue'
import Vuex from 'vuex'
import modules from './modules'

/**
 * Set Vue into Vue
 */
Vue.use(Vuex)

const store = new Vuex.Store({
  strict: true,
  modules: modules,
});

export default store;
